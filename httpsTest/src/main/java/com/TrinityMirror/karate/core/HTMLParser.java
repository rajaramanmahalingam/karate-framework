package com.TrinityMirror.karate.core;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParser {
	
	protected static String topHeaderSections = "body > header > div > nav > section > ul > li";
	protected static String allLinksOnResponse = "a[href]";
	protected static String absoluteURL = "abs:href";

	protected static Document parseResponse(String response) {
		return Jsoup.parse(response);
	}

	public static String getPageTitle(String response) throws Exception {
		Document doc = parseResponse(response);
		return doc.title();
	}
	
	public static int getCountOfHeaderSections(String response) throws Exception {
		int count = 0;
		Document doc = parseResponse(response);
		Elements headerNavigation = doc.select(topHeaderSections);
		for (Element section: headerNavigation) {
			count ++;
		}
		return count;
	}
	
	public static void getAllLinksOntheResponse(String response) throws Exception {
		Document doc = parseResponse(response);
		Elements urls = doc.select(allLinksOnResponse);
		for(Element url: urls) {
			System.out.println("\n" + url.attr(absoluteURL));
		}
	}

	public static void getChildrenOfAnySection(String response, String element) {
		Document doc = parseResponse(response);
		Element parentNode = doc.select(element).first();
		Elements childNodes = parentNode.children();
		for (Element child: childNodes) {
		 System.out.println(child.text());
		}
	}
}
	
	