Feature: Response Validations (served over HTTPS & HTTP)

  #X-True-Cache-Key: /L/org-www.southportvisiter.co.uk/ cid=service=responsive___MOBILE_DEVICE=webuser
  #X-Cache-Key: S/L/3500/570513/5m/secure.southportvisiter.co.uk/ cid=service=responsive___MOBILE_DEVICE=webuser
  #X-True-Cache-Key: /L/secure.southportvisiter.co.uk/ cid=service=responsive___MOBILE_DEVICE=webuser


  Background: 
    * configure headers = {Pragma: 'akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, akamai-x-get-cache-key, akamai-x-get-extracted-values, akamai-x-get-nonces, akamai-x-get-ssl-client-session-id, akamai-x-get-true-cache-key, akamai-x-serial-no, akamai-x-get-request-id, akamai-x-request-trace, akamai-x--meta-trace, akama-xi-get-extracted-values' }

  #Sys-519
  #Scenario - 1
  #GIVEN a URL on the sample site
  #AND where the scheme of the URL is {{https}}
  #AND a user agent with a cookie set with the name {{allowHttps}} and the value {{true}} on the {{www}} subdomain of the chosen site
  #WHEN a request is made to the URL by the user agent
  #THEN the response should be served over HTTPS and the response body should match the existing HTTP case from before this task is done
  #AND if there is a cache miss the origin used should be the secure origin of the specified site -->Yet to implement!
  #Test - 1
  Scenario: Verify if the response is 200 when the cookie allowHttps is set to 'true' and the secured url is accessed
    * url 'https://secure.southportvisiter.co.uk/'
    * def parse = Java.type('com.TrinityMirror.karate.core.HTMLParser')
    * def primaryHeaderPath = "body > header > div > nav > section > ul"
    * def links = read('Links.txt')
    Given request 'https://secure.southportvisiter.co.uk/'
    And header Accept = 'text/html'
    And cookie allowHttps = true
    When method get
    Then status 200
    # -------------------------------------- HTML Verifications -----------------------------
    # verify if the Page title from the response matches the expected requirement
    And def resultTitle = parse.getPageTitle(response)
    And match resultTitle contains 'Southport Visiter: Southport News, Southport FC'
    
    # verify if the TopNavigation section contains the expected count of items
    And assert parse.getCountOfHeaderSections(response) == 7
    
    # print all the links that are available in the response
    #And print parse.getAllLinksOntheResponse(response)

    # print all children of a particular section E.g primary
    #And print parse.getChildrenOfAnySection(response, primaryHeaderPath)
    
    # ----------------------------Response header Verifications -----------------------------
    And match responseHeaders.X-Akamai-Session-Info contains 'name=STRICT_BASELINE_V1ARL_CHECKS; value=<<ENCRYPTED>>'

  #Scenario 2:
  #GIVEN a URL on the sample site
  #AND where the scheme of the URL is https
  #AND a user agent with a cookie set with the name allowHttps with a value other than true on the www subdomain of the chosen site
  #WHEN a request is made to the URL by the user agent
  #THEN the response should be a 301 to the same URL with the http scheme
  Scenario: Verify if the response is 301 when the cookie allowHttps is set to 'false' and the secured url is accessed
    * url 'https://secure.southportvisiter.co.uk'
    Given cookie allowHttps = false
    And header Accept = 'text/html'
    And request '/'
    When method get
    Then status 301
    And match responseHeaders.Location ==  ['http://secure.southportvisiter.co.uk/']


  #Scenario 3:
  #GIVEN a URL on the sample site
  #AND where the scheme of the URL is https
  #AND a user agent with no cookie set with the name allowHttps on the www subdomain of the chosen site
  #WHEN a request is made to the URL by the user agent
  #THEN the response should be a 301 to the same URL with the http scheme
  Scenario: Verify if the response is 301 when the cookie allowHTTPS is not set and the secured url is accessed
    * url 'https://secure.southportvisiter.co.uk'
    Given request '/'
    And header Accept = 'text/html'
    When method get
    Then status 301
    And match responseHeaders.Location ==  ['http://secure.southportvisiter.co.uk/']


  #Scenario 4:
  #GIVEN a URL on the sample site
  #AND where the scheme of the URL is http
  #WHEN a request is made to the URL
  #THEN the response is not over HTTPS and works as it does before this task is done
  Scenario: Verify if the response is not over HTTPS when non-secured url is accessed
    * url 'http://secure.southportvisiter.co.uk'
    Given request '/'
    And header Accept = 'text/html'
    When method get
    Then status 200

  Scenario: Verify if the redirection does happen when non-secured url is accessed with allowHttps = true
    * url 'http://secure.southportvisiter.co.uk'
    Given cookie allowHttps = true
    And header Accept = 'text/html'
    And request '/'
    When method get
    Then status 307
    And match responseHeaders.Location ==  ['https://secure.southportvisiter.co.uk/']
